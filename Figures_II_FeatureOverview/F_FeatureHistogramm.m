%Load the file
currentFile = mfilename('fullpath');
basepath = fileparts(fileparts(currentFile)); 
f1 = load(fullfile(basepath,"Step2_featuresShape.mat"));

featuresShape = [f1.features.X(:,1:10);f1.features.X(:,11:20);f1.features.X(:,21:30);f1.features.X(:,31:40)];
featuresShapeComplex = complex(featuresShape(:,1:5),featuresShape(:,6:10));

figure('Position',[0 0 700 1500]);
subplot(2,1,1);
title("Amplitude Histogram");
hold on;
for i = 2:5
   histogram(abs(featuresShapeComplex(:,i)),'BinEdges',linspace(0,1,101),'Normalization','probability');
end
legend("Mode " + string(2:5));
xlabel("Amplitude")
ylabel("Probability")

subplot(2,1,2,polaraxes);
hold on;
title("Phase Histogram");
for i = 2:5
   plotData = angle(featuresShapeComplex(:,i));
   edges = linspace(0,2*pi,101);
   h = polarhistogram(plotData,edges,'Normalization','probability');
   hold on;
end
rlim([-0.15,0.22]);
s = get(h,'Parent');
s.RTickLabel = [];
s.ThetaAxisUnits = 'radians';
rticks([0.1 0.2])
rticklabels({'p = 0.1',' p = 0.2'})





