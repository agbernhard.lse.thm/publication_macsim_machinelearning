%Load the file
currentFile = mfilename('fullpath');
basepath = fileparts(fileparts(currentFile)); 
s1 = load(fullfile(basepath,"Step1_fourierCoefficients.mat"));

% We need to use this comparison because otherwise we loose the relative
% phase
fourierCoeffs = s1.preprocessedStruct.coeffs;
normalizedFeatures = fourierCoeffs(:,2:end)./abs(fourierCoeffs(:,2));

%Build indizes for comparison
variationCompare = {s1.preprocessedStruct.positions, ...
                    s1.preprocessedStruct.heartRate, ...
                    s1.preprocessedStruct.waveForm, ...
                    s1.preprocessedStruct.labels};
variation = cellfun(@(x) unique(x,'stable'),variationCompare,'UniformOutput',false);
variation{4} = variation{4}(~contains(variation{4},"Regression")); %remove regression;

%variationBase
variationBase = cell(4,1);
preselection = [3,1,1,6];
for i = 1:length(variation)
   variationBase{i} = variationCompare{i} == variation{i}(preselection(i));
end

titleList = ["Measurement position comparison","Heart rate comparision","Waveform comparison","Stenosis location comparison"];


figure('Position',[0 0 2000 1000]);
colors = lines(16);
%Adapt legend
legends = variation;
firstUpper = @(x) x.extractBefore(2).upper + x.extractAfter(1);
assert(legends{1}(1).contains("art")); legends{1} = arrayfun(firstUpper,legends{1});
assert(legends{2}(1).contains("HeartRate")); legends{2} = "Heart rate " + string(50:5:70)';
assert(legends{3}(1).contains("HeartWaveForm")); legends{3} = "Waveform " + string(1:10)';



t = tiledlayout(2,2);
t.TileSpacing = 'compact';
t.Padding = 'compact';
for k = 1:length(variation)
    nexttile;
    hold on;
    currentVariation = variation{k};
    handleList = [];
    for i = 1:length(currentVariation)
       index = variationCompare{k} == currentVariation(i); 
       fixedIndex = variationBase(1:length(variation) ~= k);
       for j = 1:length(fixedIndex);index = index & fixedIndex{j};end
       featuresToPlot = normalizedFeatures(index,:);
       [meanSignal,stdSignal] = getAverageSignal(featuresToPlot);
       t = 1:1000;
       patch([t fliplr(t)], [(meanSignal + stdSignal) fliplr(meanSignal - stdSignal)],...
           colors(i,:),'FaceAlpha',0.3,'EdgeColor','none');
       h = plot(t,meanSignal,'Color',colors(i,:));
       handleList(i) = h;
    end
    legend(handleList,legends{k},'location','northeast','FontSize',15);
    
    title(titleList(k),'FontSize',16);
    xlabel("Signal Phase",'FontSize',14);
    ylabel("Normalized Amplitude",'FontSize',14);
    xticks([1 250 500 750 1000])
    xticklabels({'-\pi','-\pi/2','0','\pi/2','\pi'})
    ax=gca;
    ax.FontSize = 16;
end

%% Common used logic
function [meanSignal,stdSignal] = getAverageSignal(featuresToPlot)
   signalsToPlot = zeros(5,1000);
   for j = 1:5
        shiftedFeatures = PublicationTools.shiftByReference(...
            exp(complex(0,pi/2))*featuresToPlot(j,1),[0 featuresToPlot(j,:)]);
        signalsToPlot(j,:) = PublicationTools.fftToSignal(shiftedFeatures,1000);
   end
   meanSignal = mean(signalsToPlot);
   stdSignal  = std(signalsToPlot);
end



