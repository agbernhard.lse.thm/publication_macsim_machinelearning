%% This script runs without any
rate = 1000;
basepath = fileread('DatasetPath.txt'); 
%only load 1 Data to show the results
signalStruct1 = MacSimDataLoader.getData(fullfile(basepath,"Dataset"),[4],[1],[1],[1]);

%create filter
fs = 1000;
fc_l = 15; width_l = 0.1; attenuation_l = 30;

lpFilt = designfilt('lowpassfir','PassbandFrequency',fc_l/fs*2, ...
      'StopbandFrequency',(fc_l+width_l*fc_l)/fs*2,'PassbandRipple',0.5, ...
      'StopbandAttenuation',attenuation_l,'DesignMethod','kaiserwin');

% One figure with all signals below each other
tStart = 15;
tEnd = 25.5;

colors = lines(3);

dt = (tEnd-tStart);
t = linspace(tStart,tEnd-1/rate,rate*dt);
f1 = figure('Position',[0 0 700 400]);
hold on;
signalRaw = signalStruct1.data(tStart*rate:(tEnd*rate-1));
signal = filtfilt(lpFilt,signalRaw);
title(signalStruct1.anatomicalPosition);
plot(t,signalRaw);
plot(t,signal);

[coeff,f] = PublicationTools.fitFourierAutomatic(signal,rate,5,true);
plot(t,PublicationTools.fftToPeriodicSignal(coeff,f,t-tStart)); 

xlim([17,20]);
ylim([20 140]);
xlabel("t/s")
ylabel("p/mmHg")
legend(["Raw signal","Lowpass filtered","Fourier series"],'Location','southeast');












