# Publication MACSim Machine Learning

Contains all code needed to reproduce the publication results and figures (if they are Matlab generated).

Publication: https://doi.org/10.1016/j.compbiomed.2022.106224

To execute the code:
* Download the dataset and tools from Zenodo: http://doi.org/10.5281/zenodo.6421497
* Unzip the dataset and the tools
* Put the datset directory in DatasetPath.txt
* Add the tools to the Matlab path

Afterwards you can execute step 1 - 4 and the code in the subdirectories to produce the figures. 
Note that the first step is lengthy (takes about 1 hour).
The names Figures_I - Figures_IV do not correspond to the step numbers, but give a similar order as used in the paper.

