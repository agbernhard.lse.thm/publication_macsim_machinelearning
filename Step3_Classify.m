% For reproducibility always initialize the seed
rng(20220406);
%https://de.mathworks.com/matlabcentral/answers/17118-different-neural-network-training-result-each-time
RandStream.setGlobalStream (RandStream ('mrg32k3a','Seed',20220406));


%% Load the data and select only the classification relevant 
currentFile = mfilename('fullpath');
basepath = fileparts(currentFile); 
fShape = load(fullfile(basepath,"Step2_featuresShape.mat"));
fTransfer = load(fullfile(basepath,"Step2_featuresTransfer.mat"));

%The following logic assumes that so we have to make sure this matches
assert( all(fShape.features.waveForm == fTransfer.features.waveForm) & ...
        all(fShape.features.heartRate == fTransfer.features.heartRate) & ...
        all(fShape.features.labels == fTransfer.features.labels), ...
        "Some of the attributes do not have the same order.")


selectionClassification = ~contains(fShape.features.labels,"Regression");
labels = fShape.features.labels(selectionClassification);
heartRate = fShape.features.heartRate(selectionClassification);
waveForm = fShape.features.waveForm(selectionClassification);


labelsAll = unique(labels,'stable');
heartRateAll = unique(heartRate,'stable');
waveFormAll = unique(waveForm,'stable');


featuresShape = fShape.features.X(selectionClassification,:);
featuresShape(:,[1,6,11,16,21,26,31,36]) = []; %Remove features without information
featuresTransfer = fTransfer.features.X(selectionClassification,:);


featureList = {featuresShape,featuresTransfer};
classifierHandles = {@classifySVM,@classifyKNN,@classifyNeuralNet,@classifyRandomForest,@classifyTreeBoosting};

%% Heart Rate cross validation
testIndexListHR = cell(length(heartRateAll),1);
for i = 1:length(heartRateAll)
    testIndexListHR{i} = heartRateAll(i) == heartRate;
end

resultsHR = struct();
for i = 1:length(classifierHandles)
    resultsHR(i).classifier = string(char(classifierHandles{i}));
    [confHeartRateTest,accHR] = performCVall(classifierHandles{i},featureList,testIndexListHR,labels,labelsAll);
    c_HR_sum = sum(confHeartRateTest,1);
    resultsHR(i).c_HR_shape = squeeze(c_HR_sum(1,1,:,:));
    resultsHR(i).c_HR_transfer = squeeze(c_HR_sum(1,2,:,:));
    resultsHR(i).accHR = accHR;
end

%% Wave form cross validation
testIndexListWF = cell(length(waveFormAll)/2,1);
for i = 1:2:length(waveFormAll)
    %only 5-fold over waveform to
    indArr = 1+(i-1)/2;
    testIndexListWF{indArr} = waveFormAll(i) == waveForm | ...
                         waveFormAll(i+1) == waveForm;
end

resultsWF = struct();
for i = 1:length(classifierHandles)
    resultsWF(i).classifier = string(char(classifierHandles{i}));
    [confWaveFormTest,accWF] = performCVall(classifierHandles{i},featureList,testIndexListWF,labels,labelsAll);
    c_WF_sum = sum(confWaveFormTest,1);
    resultsWF(i).c_WF_shape = squeeze(c_WF_sum(1,1,:,:));
    resultsWF(i).c_WF_transfer = squeeze(c_WF_sum(1,2,:,:));
    resultsWF(i).accWF = accWF;
end

save(fullfile(basepath,"Step3_classificationResults.mat"),'labelsAll',...
    'heartRateAll','resultsHR','resultsWF');


%% General function for cross validation with all features
function [c4d,acc] = performCVall(classifierHandle,featureList,testIndexList,labels,labelsAll)
    c4d = zeros(length(testIndexList),length(featureList),length(labelsAll),length(labelsAll));
    acc = zeros(length(testIndexList),length(featureList));
    for i = 1:length(testIndexList)
        indexTest = testIndexList{i};
        indexTrain = ~indexTest;   
        for j = 1:length(featureList)
            [predTrain,predTest] = classifierHandle(featureList{j}(indexTrain,:),...
                                               featureList{j}(indexTest,:),...
                                               labels(indexTrain),...
                                               labels(indexTest));
            c4d(i,j,:,:) = confusionmat(labels(indexTest),predTest,'Order',labelsAll);
            
            confSub = squeeze(c4d(i,j,:,:));
            acc(i,j) = sum(diag(confSub))/sum(confSub,'all');           
        end
    end
end


function [predTrain, predTest] = classifySVM(trainingX,testX,trainingY,testY)
    %% Taken from classification learner
    template = templateSVM(...
        'KernelFunction', 'linear', ...
        'PolynomialOrder', [], ...
        'KernelScale', 'auto', ...
        'BoxConstraint', 1, ...
        'Standardize', true);
    classificationSVM = fitcecoc(...
        trainingX, ...
        trainingY, ...
        'Learners', template, ...
        'Coding', 'onevsone');

    [predTrain,~] = predict(classificationSVM,trainingX);
    [predTest,~] = predict(classificationSVM,testX);
end

function [predTrain, predTest] = classifyKNN(trainingX,testX,trainingY,testY)
    classificationKNN = fitcknn(trainingX,trainingY, ...
        'NumNeighbors',10,...
        'DistanceWeight','equal');

    [predTrain,~] = predict(classificationKNN,trainingX);
    [predTest,~] = predict(classificationKNN,testX);    
end

function [predTrain, predTest] = classifyNeuralNet(trainingX,testX,trainingY,testY)
    
    %labels to matrix (boilerplate)
    uniqueLabels = unique([trainingY;testY],'stable');
    matrixTrainingY = zeros(length(trainingY),length(uniqueLabels));
    for i = 1:length(uniqueLabels)
       matrixTrainingY(:,i) = trainingY == uniqueLabels(i); 
    end
    
    %Important part
    net = patternnet([20]);
    net.trainParam.showWindow = false;
    net.trainParam.max_fail = 100;
    net.trainParam.epochs = 10000;
    
    net = train(net,trainingX',matrixTrainingY');
    predTrainMatrix = net(trainingX')';
    predTestMatrix = net(testX')';
    
    %matrix to labels again (boilerplate)    
    [~,indPredTrain] = max(predTrainMatrix,[],2);
    [~,indPredTest] = max(predTestMatrix,[],2);
    predTrain = strings(size(trainingY));
    predTest = strings(size(testY));
    for i = 1:size(trainingY,1)
       predTrain(i) = uniqueLabels(indPredTrain(i)); 
    end    
    for i = 1:size(testY,1)
       predTest(i) = uniqueLabels(indPredTest(i)); 
    end         
end

function [predTrain, predTest] = classifyRandomForest(trainingX,testX,trainingY,testY)
    Mdl = TreeBagger(1000,trainingX,trainingY,'Method','classification');   

    [predTrain,~] = predict(Mdl,trainingX);
    [predTest,~] = predict(Mdl,testX);    
end

function [predTrain, predTest] = classifyTreeBoosting(trainingX,testX,trainingY,testY)
    Mdl = fitcensemble(trainingX,trainingY,'Method','AdaBoostM2','Learners','tree');    
    [predTrain,~] = predict(Mdl,trainingX);
    [predTest,~] = predict(Mdl,testX);    
end

% Matlab xgboost library (File-Exchange) 
% https://de.mathworks.com/matlabcentral/fileexchange/75898-functions-to-run-xgboost-in-matlab
% Instead of building the dll I installed it in Anaconda3, which does that
% automatically with:
% conda install -c anaconda py-xgboost 
% Than I downloaded the header file as described in File Exchange and put
% it in the same directory as xgboost.dll build by anaconda. Than I changed
% the path to the dll in said files



