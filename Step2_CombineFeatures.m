% For reproducibility always initialize the seed
rng(20220406);

%Load the file
currentFile = mfilename('fullpath');
basepath = fileparts(currentFile); 
s1 = load(fullfile(basepath,"Step1_fourierCoefficients.mat"));

%Create arrays and normalize features
matData = s1.preprocessedStruct.coeffs;
featuresNormalized = matData(:,2:end)./abs(matData(:,2));

%Base array
featuresBase = struct();
featuresBase.labels = s1.preprocessedStruct.labels(1:4:end);
featuresBase.waveForm = s1.preprocessedStruct.waveForm(1:4:end);
featuresBase.heartRate = s1.preprocessedStruct.heartRate(1:4:end);


%% Create shape features
features = featuresBase;
featuresDescription = "Real RS, Imag RS," + ...
                      "Real RD, Imag RD," + ...
                      "Real TD, Imag TD," + ...
                      "Real TS, Imag TS," + ...
                      "Mean(freq)";

featuresShifted = zeros(size(featuresNormalized));
for i = 1:size(matData,1)
    tempShifted = PublicationTools.shiftByReference(featuresNormalized(i,1),[0 featuresNormalized(i,:)]);
    featuresShifted(i,:) = tempShifted(2:end);
end


indexP1 = s1.preprocessedStruct.positions == "art. radialis sinistra";
indexP2 = s1.preprocessedStruct.positions == "art. radialis dextra";
indexP3 = s1.preprocessedStruct.positions == "art. tibialis anterior dextra";
indexP4 = s1.preprocessedStruct.positions == "art. tibialis anterior sinistra";

features.X = [real(featuresShifted(indexP1,:)), imag(featuresShifted(indexP1,:)), ...
                          real(featuresShifted(indexP2,:)), imag(featuresShifted(indexP2,:)), ...
                          real(featuresShifted(indexP3,:)), imag(featuresShifted(indexP3,:)), ...
                          real(featuresShifted(indexP4,:)), imag(featuresShifted(indexP4,:)), ...
                          mean(reshape(s1.preprocessedStruct.frequencies,4,[]))'];
save(fullfile(basepath,"Step2_featuresShape.mat"),'features','featuresDescription');


%% Create transfer function features
features = featuresBase;
featuresDescription = "Real RSTS, Imag RSTS," + ...
                       "Real RDTD, Imag RDTD," + ...
                       "Mean(freq)";


indexP1 = s1.preprocessedStruct.positions == "art. radialis sinistra";
indexP2 = s1.preprocessedStruct.positions == "art. radialis dextra";
indexP3 = s1.preprocessedStruct.positions == "art. tibialis anterior dextra";
indexP4 = s1.preprocessedStruct.positions == "art. tibialis anterior sinistra";

transfer1 = featuresNormalized(indexP1,:)./featuresNormalized(indexP4,:);
transfer2 = featuresNormalized(indexP2,:)./featuresNormalized(indexP3,:);
transfer3 = featuresNormalized(indexP1,:)./featuresNormalized(indexP2,:);
transfer4 = featuresNormalized(indexP4,:)./featuresNormalized(indexP3,:);


features.X = [real(transfer1), imag(transfer1), ...
              real(transfer2), imag(transfer2), ...
              real(transfer3), imag(transfer3), ...
              real(transfer4), imag(transfer4), ...
              mean(reshape(s1.preprocessedStruct.frequencies,4,[]))'];

save(fullfile(basepath,"Step2_featuresTransfer.mat"),'features','featuresDescription');