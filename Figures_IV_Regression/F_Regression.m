currentFile = mfilename('fullpath');
basepath = fileparts(fileparts(currentFile)); 
load(fullfile(basepath,"Step4_regressionResults.mat"));


lineStyles = {'x-.','x-','x:','x--'};
colors = lines(10);
assert(length(resultsHR) < 10, "not enough colors defined");

indexDist = [resultsWF.regressionHandle] == "regressionNeuralNet";
%% Distribution plot
allLabels = unique(labelsRegression,'stable');
binNumber = 2000;
allDistributions = zeros(length(allLabels),binNumber);
allMean = zeros(length(allLabels),1);
allStd = zeros(length(allLabels),1);
genericX = linspace(-10,40,binNumber);

for i = 1:length(allLabels)
    predLabel_i = resultsWF(indexDist).predArrWF(labelsRegression == allLabels(i),2);
    fit_dist = fitdist(predLabel_i,'Kernel');
    allDistributions(i,:) = fit_dist.pdf(genericX);
end

figure('Position',[0 0 700 1700]);
maxval = 0.1*ceil(10*max(reshape(allDistributions,1,[])));
for i = 1:length(allLabels)
    subplot(10,1,i);
    hold on;
    plot(genericX,allDistributions(i,:),'k');
    plot(repmat(allLabels(i),1,100),linspace(0,maxval,100),'k');
    plot(repmat(allLabels(i)-2,1,100),linspace(0,maxval,100),'Color',[0.8 0.8 0.8]);
    plot(repmat(allLabels(i)+2,1,100),linspace(0,maxval,100),'Color',[0.8 0.8 0.8]);
    xlim([2,24]);
    ylim([0,maxval]);
    ylabel("Probability");
end
xlabel("Position in cm");

%% Overview plot HR
facc = figure('Position',[0 0 700 700]);
axacc = axes(facc);
hold on;

legendArr = [];
for i = 1:length(resultsHR)
    plot(axacc,resultsHR(i).rmseArrHR(:,1),lineStyles{1},'MarkerSize',10,'Color',colors(i,:));
    plot(axacc,resultsHR(i).rmseArrHR(:,2),lineStyles{2},'MarkerSize',10,'Color',colors(i,:));
    legendArr = [legendArr, resultsHR(i).regressionHandle + " shape feat.",...
                            resultsHR(i).regressionHandle + " transfer feat.",...
                            ];
end

plot(1:5,repmat(2,1,5),'kx-');
ylim([0,40]);
xlim([0.8,5.2]);
ylabel("RMSE (cm)")
axacc.YGrid = 'on';
xtickangle(40)
xticks(1:5)
xticklabels(cellstr("Heart rate " + string(50:5:70)))
legend([legendArr "Training distance"],'location','northwest');


%% Overview plot WF
facc2 = figure('Position',[0 0 700 700]);
axacc2 = axes(facc2);
hold on;

legendArr = [];
for i = 1:length(resultsWF)
    plot(axacc2,resultsWF(i).rmseArrWF(:,1),lineStyles{1},'MarkerSize',10,'Color',colors(i,:));
    plot(axacc2,resultsWF(i).rmseArrWF(:,2),lineStyles{2},'MarkerSize',10,'Color',colors(i,:));
    legendArr = [legendArr, resultsWF(i).regressionHandle + " shape feat.",...
                            resultsWF(i).regressionHandle + " transfer feat.",...
                            ];
end
plot(1:5,repmat(2,1,5),'kx-');
ylim([0,15]);
xlim([0.8,5.2]);
ylabel("RMSE (cm)")
axacc2.YGrid = 'on';
xtickangle(40)
xticks(1:5)
xticklabels(cellstr("Waveform " + string(1:2:10) + " and " + string(2:2:10)))
legend([legendArr "Training distance"],'location','northwest');

 


