%Load the file
currentFile = mfilename('fullpath');
basepath = fileparts(fileparts(currentFile)); 
f1 = load(fullfile(basepath,"Step2_featuresShape.mat"));
f2 = load(fullfile(basepath,"Step2_featuresTransfer.mat"));

selectionRegression = contains(f1.features.labels,"Regression");

featuresList = {};
featuresList{1} = complex(f1.features.X(selectionRegression,21:25), ....
                          f1.features.X(selectionRegression,26:30)); % Location of TD
featuresList{2} = complex(f2.features.X(selectionRegression,11:15), ...
                          f2.features.X(selectionRegression,16:20)); % Location of RDTD

cm = colormap('parula');
downsampleJump = floor(size(cm,1)/10);
colors = cm(1:downsampleJump:end,:);
 

regressionLabels = unique(f1.features.labels(selectionRegression),'stable');

                        
figure('Position',[0 0 1500 600]);

for m = 1:2
    for i = 2:5
        subplot(2,4,i-1+(m-1)*4);
        if m == 1
            title("Shape $\hat{c}'_{TS}$ for n = " + string(i),'Interpreter','latex');
        else
            title("Transfer $h_{RSTS}$ for n = " + string(i),'Interpreter','latex');
        end
        hold on;
        for j = 1:length(regressionLabels)
           index = f1.features.labels == regressionLabels(j);
           currentFeatures = featuresList{m}(index,i);
           meanVals = mean(reshape(currentFeatures,5,[]));
           meanReal = real(meanVals);
           meanImag = imag(meanVals); 
           
           plot(meanReal,meanImag,'.','Color',colors(j,:));
           xlabel("Real")
           ylabel("Imag")
        end
    end
end




