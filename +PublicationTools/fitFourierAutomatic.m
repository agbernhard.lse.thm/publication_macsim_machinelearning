function [coefficients,frequency] = fitFourierAutomatic(signal,samplingRate,modes,fitNonlinear)
    %% This function does at least expect a baseline corrected signal!
    assert(any(size(signal) == 1) & length(size(signal)) < 3, ...
        "Signal must be shaped 1 x n or n x 1");
    assert(modes < 100,"Can not  deal with more than 100 nodes");
    
    signal = reshape(signal,[],1);
    time = linspace(0,length(signal)/samplingRate,length(signal))';
    
    %get initial guess for the frequency
    signalFft = fft(signal);
    signalHalf = floor(length(signal)/2);
    circularAutoCorrelation = real(ifft(signalFft.*conj(signalFft)));
    maxSearch = circularAutoCorrelation(2:signalHalf);
    lowerBound = 30/60*samplingRate; %Puls of 30 should be minimum
    maxSearch(1:lowerBound) = 0;
    [~,maxIndex] = max(maxSearch);
    frequency = samplingRate/maxIndex;
    
    [a0,an,bn] = extractStartingCoefficients(time,signal,frequency,modes);
    
    coefficients = complex(a0)*2;
    coefficients = [coefficients; complex(an,-bn)]./2;
    
    if fitNonlinear
        %Now try to improve the fit with a nonlinear method
        %https://de.mathworks.com/matlabcentral/answers/521629-fourier-s-series-fitting-problem
        %modified the above
        fitString = "a" + num2str(0,'%02d');
        for i = 1:modes
           fitString = fitString + "+ a" + num2str(i,'%02d') + "*cos(w*" + string(i) + "*x" + ")";
           fitString = fitString + "+ b" + num2str(i,'%02d') + "*sin(w*" + string(i) + "*x" + ")";   
        end
        ft = fittype(char(fitString));
        startingPoint = [a0 an' bn' frequency*2*pi];
        lowerBound = [-Inf(1,modes*2+1) frequency*2*pi*0.8];
        upperBound = [Inf(1,modes*2+1) frequency*2*pi*1.2];
        fit_model = fit(time, signal, ft,...
                        'Algorithm', 'Trust-Region',...
                        'Lower', lowerBound,...
                        'Upper', upperBound,...
                        'StartPoint', startingPoint);
        coefffit = coeffvalues(fit_model)';
        a0new = coefffit(1);
        annew = coefffit(2:(modes+1));
        bnnew = coefffit((modes+2):(end-1));
        fnew = coefffit(end)/2/pi;

        coefficientsNew = complex(a0new)*2;
        coefficientsNew = [coefficientsNew; complex(annew,-bnnew)]./2;
        
        coefficients = coefficientsNew;
        frequency = fnew;
        
    end

end


function [a0,an,bn] = extractStartingCoefficients(time,signal,frequency,modes)
    
    %% This function fits a simple Fourier Series using the normal equation   
    X = zeros(length(time),2*modes+1);
    Y = signal;

    X(:,1) = 1; %This is a0
    for i = 1:modes
        %now for a1 to an
        indexCos = 1 + i;
        X(:,indexCos) = cos(time*frequency*2*pi*i);
        %same for b1 to bn
        indexSin = 1 + i + modes;
        X(:,indexSin) = sin(time*frequency*2*pi*i);
    end
    
    params = (X'*X)^(-1)*X' * Y;
    
    a0 = params(1);
    an = params(2:(modes+1));
    bn = params((modes+2):end);
end

