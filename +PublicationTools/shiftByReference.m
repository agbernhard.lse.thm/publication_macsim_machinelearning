function fftArray = normalizePhasesByReference(complexReference,fftArray)
    for i = 2:length(fftArray)
       %This aligns the phase with respect to the reference Value
       r = i-1;
       fftArray(i) = fftArray(i)/(complexReference^r)*abs(complexReference)^r;
    end
end

