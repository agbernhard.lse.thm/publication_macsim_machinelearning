function signalPeriod = fftToSignal(fftArrayIn,signalLength)
    sizeInput = size(fftArrayIn);
    if sizeInput(1) > sizeInput(2)
       fftArray = conj(fftArrayIn'); %Matlab implicitly takes the conjugate
    else
       fftArray = fftArrayIn; 
    end
    newFftArray = zeros(signalLength,1);
    lin = length(fftArray);
    newFftArray(1:lin) = fftArray;
    newFftArray((end-lin+2):end) = fliplr(conj(fftArray(2:end)));
    signalPeriod = real(ifft(newFftArray))*signalLength;
end

