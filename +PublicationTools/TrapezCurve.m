function HeartPressureArrayInMilimeterHg = TrapezCurve(HeartrateInBpm, HeartPressureAmplitudeInMilimeterHg, PressureOffsetInMilimeterHg,varargin)

    %% Trapez Curve
    % This is the curve generated as input for the ventricel
    %% Usage example
    % TrapezCurve(50,220,-100)
    
    % Publication relevant:
    % Parameters range from  HeartRateInBPM 50-70
    % ...HeartPressureAmplitudeInMilimeterHg 220-250
    % ...PressureOffsetInMilimeterHg -100
    % (varargin) tStartAscend = 0.1; tEndAscend = 0.15; tStartDescend =
    % 0.45; tEndDescend = 0.9;

    
    %Set standard parameters
    %values are in fraction of the intervall
    tStartAscend = 0.1;
    tEndAscend = 0.15;
    tStartDescend = 0.45;
    tEndDescend = 0.9;

    %values used for gaussian
    windowFraction = 0.1;

    if nargin > 3
        assert(nargin == 4,"optional parameters should be passed as struct");                
        try
            structPara = varargin{1};
            tStartAscend = structPara.tStartAscend;
            tEndAscend = structPara.tEndAscend;
            tStartDescend = structPara.tStartDescend;
            tEndDescend = structPara.tEndDescend;
            windowFraction = structPara.windowFraction;
        catch
           error("Could not parse parameters"); 
        end
    end

    HRFactor = 10/HeartrateInBpm; %Factor between the default heartrate and the current heartrate
    % this last measuring point will be deleted later.
    MeasuringPoints = floor(6000*(HRFactor))+1; 

    tAxis = linspace(0,1,MeasuringPoints);

    % slope and shift of linear segments
    slopeAscend = HeartPressureAmplitudeInMilimeterHg/(tEndAscend-tStartAscend);
    shiftAscend =  - slopeAscend*tStartAscend;
    slopeDescend = -HeartPressureAmplitudeInMilimeterHg/(tEndDescend-tStartDescend);
    shiftDescend =  - slopeDescend*tEndDescend;           

    %This  logic creates  the rectangle           
    HeartValue = PressureOffsetInMilimeterHg + ...
           (tAxis  > tStartAscend & tAxis <= tEndAscend) .* (slopeAscend*tAxis+shiftAscend) + ...
           (tAxis  > tEndAscend & tAxis <= tStartDescend) .* HeartPressureAmplitudeInMilimeterHg + ...
           (tAxis  > tStartDescend & tAxis <= tEndDescend) .* (slopeDescend*tAxis+shiftDescend);

    HeartValueSmooth = smoothdata(HeartValue,'gaussian',floor(windowFraction*MeasuringPoints));
    HeartPressureArrayInMilimeterHg = HeartValueSmooth(1:(length(HeartValueSmooth)-1));
end
