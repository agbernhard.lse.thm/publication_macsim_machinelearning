function signal = fftToPeriodicSignal(coeff,frequency,time)
    signal = coeff(1);
    for i = 2:length(coeff)
        signal = signal + ...
            real(coeff(i)*exp(complex(0,2*pi*(i-1)*time*frequency)) + ...
                 coeff(i)'*exp(complex(0,-2*pi*(i-1)*time*frequency)));
    end
end