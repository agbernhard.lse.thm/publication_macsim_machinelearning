% For reproducibility always initialize the seed
rng(20220406);
%https://de.mathworks.com/matlabcentral/answers/17118-different-neural-network-training-result-each-time
RandStream.setGlobalStream (RandStream ('mrg32k3a','Seed',20220406));

%% Load the data and select only the classification relevant 
currentFile = mfilename('fullpath');
basepath = fileparts(currentFile); 
fShape = load(fullfile(basepath,"Step2_featuresShape.mat"));
fTransfer = load(fullfile(basepath,"Step2_featuresTransfer.mat"));

%The following logic assumes that so we have to make sure this matches
assert( all(fShape.features.waveForm == fTransfer.features.waveForm) & ...
        all(fShape.features.heartRate == fTransfer.features.heartRate) & ...
        all(fShape.features.labels == fTransfer.features.labels), ...
        "Some of the attributes do not have the same order.")

selectionRegression = contains(fShape.features.labels,"Regression");
labels = fShape.features.labels(selectionRegression);
labelsRegression = 24 - 2*double(arrayfun(@(x) x.extractAfter(28),labels)); %Distance from reference points

heartRate = fShape.features.heartRate(selectionRegression);
waveForm = fShape.features.waveForm(selectionRegression);

labelsRegressionAll = unique(labelsRegression,'stable');
heartRateAll = unique(heartRate,'stable');
waveFormAll = unique(waveForm,'stable');

featuresShape = fShape.features.X(selectionRegression,:);
featuresShape(:,[1,6,11,16,21,26,31,36]) = []; %Remove empty information
featuresTransfer = fTransfer.features.X(selectionRegression,:);

featureList = {featuresShape,featuresTransfer};


regressionHandles = {@regressionLinear,@regressionLinearInteraction,@regressionNeuralNet};

%% Heart Rate cross validation
testIndexListHR = cell(length(heartRateAll),1);
for i = 1:length(heartRateAll)
    testIndexListHR{i} = heartRateAll(i) == heartRate;
end
resultsHR = struct();
for i = 1:length(regressionHandles)
    resultsHR(i).regressionHandle = string(char(regressionHandles{i}));
    [rmseArrHR,predArrHR] = performCVall_reg(regressionHandles{i},featureList,testIndexListHR,labelsRegression,labelsRegressionAll);
    resultsHR(i).rmseArrHR = rmseArrHR;
    resultsHR(i).predArrHR = predArrHR;
end
%% Wave form cross validation
testIndexListWF = cell(length(waveFormAll)/2,1);
for i = 1:2:length(waveFormAll)
    %only 5-fold over waveform to
    indArr = 1+(i-1)/2;
    testIndexListWF{indArr} = waveFormAll(i) == waveForm | ...
                         waveFormAll(i+1) == waveForm;
end

resultsWF = struct();
for i = 1:length(regressionHandles)
    resultsWF(i).regressionHandle = string(char(regressionHandles{i}));
    [rmseArrWF,predArrWF] = performCVall_reg(regressionHandles{i},featureList,testIndexListWF,labelsRegression,labelsRegressionAll);
    resultsWF(i).rmseArrWF = rmseArrWF;
    resultsWF(i).predArrWF = predArrWF;
end

save(fullfile(basepath,"Step4_regressionResults.mat"),"resultsWF","resultsHR","labelsRegression","waveFormAll","heartRateAll");

%% General function for cross validation with all features
function [rmseArr,predArr] = performCVall_reg(regressionHandle,featureList,testIndexList,labels,labelsAll)
    rmseArr = zeros(length(testIndexList),length(featureList));
    predArr = zeros(length(labels),length(featureList));
    for i = 1:length(testIndexList)
        indexTest = testIndexList{i};
        indexTrain = ~indexTest;   
        for j = 1:length(featureList)
            [predTest,predTrain] = regressionHandle(featureList{j}(indexTrain,:),...
                                               featureList{j}(indexTest,:),...
                                               labels(indexTrain),...
                                               labels(indexTest));
            predArr(indexTest,j) = predTest;           
            rmseArr(i,j) = sqrt(mean(((predTest-labels(indexTest)).^2))); 
        end
    end
end


function [predTest,predTrain] = regressionLinear(trainingX,testX,trainingY,testY)
    
    linearModel = fitlm(trainingX,trainingY,'linear');

    predTest = predict(linearModel,testX);
    predTrain = predict(linearModel,trainingX);
end

function [predTest,predTrain] = regressionLinearInteraction(trainingX,testX,trainingY,testY)
    
    linearModel = fitlm(trainingX,trainingY,'interactions');

    predTest = predict(linearModel,testX);
    predTrain = predict(linearModel,trainingX);
end


function [predTest,predTrain] = regressionNeuralNet(trainingX,testX,trainingY,testY)
    % Minimal syntax neural net
    net = fitnet([20]);
    net.trainParam.showWindow = false;
    net.trainParam.max_fail = 100;
    net.trainParam.epochs = 10000;
    
    net = train(net,trainingX',trainingY');
    predTest = net(testX')';
    predTrain = net(trainingX')';
end

