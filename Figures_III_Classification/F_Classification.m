currentFile = mfilename('fullpath');
basepath = fileparts(fileparts(currentFile)); %Not pretty
load(fullfile(basepath,"Step3_classificationResults.mat"));

lineStyles = {'x-.','x-','x:','x--'};
colors = lines(10);
assert(length(resultsHR) < 10, "not enough colors defined");

%% Overview plot HR
facc = figure('Position',[0 0 700 700]);
axacc = axes(facc);
hold on;

legendArr = [];
for i = 1:length(resultsHR)
    plot(axacc,resultsHR(i).accHR(:,1)*100,lineStyles{1},'MarkerSize',10,'Color',colors(i,:));
    plot(axacc,resultsHR(i).accHR(:,2)*100,lineStyles{2},'MarkerSize',10,'Color',colors(i,:));
    legendArr = [legendArr, resultsHR(i).classifier + " shape feat.",...
                            resultsHR(i).classifier + " transfer feat.",...
                            ];
end

plot(1:5,repmat(100/6,1,5),'kx-');
ylim([0,100]);
xlim([0.8,5.2]);
ylabel("Accuracy")
axacc.YGrid = 'on';
xtickangle(40)
xticks(1:5)
xticklabels(cellstr("Heart rate " + string(50:5:70)))
legend([legendArr "Baseline accuracy"],'location','southwest');

%% Confusion Chart only for WF and
figure('Position',[0 0 1200 500]);
indexSVM = [resultsWF.classifier] == "classifySVM";
confusionchart(resultsWF(indexSVM).c_WF_transfer,labelsAll,'RowSummary','row-normalized');


%% Overview plot WF
facc2 = figure('Position',[0 0 700 700]);
axacc2 = axes(facc2);
hold on;

legendArr = [];
for i = 1:length(resultsWF)
    plot(axacc2,resultsWF(i).accWF(:,1)*100,lineStyles{1},'MarkerSize',10,'Color',colors(i,:));
    plot(axacc2,resultsWF(i).accWF(:,2)*100,lineStyles{2},'MarkerSize',10,'Color',colors(i,:));
    legendArr = [legendArr, resultsWF(i).classifier + " shape feat.",...
                            resultsWF(i).classifier + " transfer feat.",...
                            ];
end

plot(1:5,repmat(100/6,1,5),'kx-');
ylim([0,100]);
xlim([0.8,5.2]);
ylabel("Accuracy")
axacc2.YGrid = 'on';
xtickangle(40)
xticks(1:5)
xticklabels(cellstr("Waveform " + string(1:2:10) + " and " + string(2:2:10)))
legend([legendArr "Baseline accuracy"],'location','southwest');
