%Load the file
currentFile = mfilename('fullpath');
basepath = fileparts(fileparts(currentFile));
f1 = load(fullfile(basepath,"Step2_featuresShape.mat"));
f2 = load(fullfile(basepath,"Step2_featuresTransfer.mat"));

selectionClassification = ~contains(f1.features.labels,"Regression");

featuresList = {};
featuresList{1} = complex(f1.features.X(selectionClassification,31:35), ....
                          f1.features.X(selectionClassification,36:40)); % Location of TS
featuresList{2} = complex(f2.features.X(selectionClassification,11:15), ...
                          f2.features.X(selectionClassification,16:20)); % Location of RDTD
                        
figure('Position',[0 0 1500 400]);
titleList = ["Signal shape $\hat{c}'_{TS}$ for n = 2 ","Transfer function $h_{RDTD}$ for n = 2 "];
legendList = unique(f1.features.labels(selectionClassification),'stable');
subplot(1,3,1)
plot(zeros(2,6))
axis off
legend(legendList,'Location','westoutside');
colors = lines(16);
for m = 1:2
    features = featuresList{m};
    for k = 2:2
        subplot(1,3,(m-1)*1+k)
        hold on;
        handleList = [];
        %For each measurement Set
        for i = 1:6  
            %Get the means for this block
            indexStart = 1+(i-1)*5*5*10;
            indexEnd = i*5*5*10;
            currentFeatures = features(indexStart:indexEnd,k);
            meanVals = mean(reshape(currentFeatures,5,[]));
            meanReal = real(meanVals);
            meanImag = imag(meanVals);           
            %For each Frequency
            for j = 1:5
                fStart = 1+(j-1)*10; 
                fEnd =  j*10;
                plot(meanReal(fStart:fEnd),meanImag(fStart:fEnd),'o-','color',colors(i,:));
            end
        end
        xlabel("Real part")
        ylabel("Imag part")
        title(titleList(m),'Interpreter','latex');
    end
end






