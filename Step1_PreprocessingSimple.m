% For reproducibility always initialize the seed
rng(20220406);

basepath = fileread('DatasetPath.txt'); 
signalStruct = MacSimDataLoader.getData(fullfile(basepath,"Dataset"),[4,11,14,13],[],[],[]);
rate = 1000;

%Perform data augmentation by splitting the data
numberSignals = 4;
numberSplits = 5;
modes = 5;
dataSize = size(signalStruct,1)*numberSplits;

%Only preprocessing is fitting the fourier modes
coeffs = zeros(dataSize,modes+1);
frequencies = zeros(dataSize,1);
labels = strings(dataSize,1);
positions = strings(dataSize,1);
heartRate = strings(dataSize,1);
waveForm = strings(dataSize,1);

meanAbsoluteError = zeros(dataSize,1);
signalSpan = zeros(dataSize,1);
signalToNoiseRatio = zeros(dataSize,1);

tic;
for i = 1:(dataSize/numberSplits)
    for j = 1:numberSplits
        indexNew = numberSplits*(i-1)+j;
        startIndex = rate*(5 + (j-1)*10.5);
        endIndex = rate*(5 + j*10.5)-1;
                    
        signal = signalStruct(i).data(startIndex:endIndex);
        time = linspace(5 + (j-1)*10,(5 + j*10)-1/rate,5*rate);
        [coeff,f] = PublicationTools.fitFourierAutomatic(signal,rate,modes,true);
                
        %error estimate
        tFit = linspace(0,length(signal)/rate,length(signal));
        signalFit = PublicationTools.fftToPeriodicSignal(coeff,f,tFit);
        meanAbsoluteError(indexNew) = mean(abs(signalFit-signal));
        signalSpan(indexNew) = max(signalFit)-min(signalFit); 
        
        %unbiased snr
        signalToNoiseRatio(indexNew) = snr(signal-mean(signal));
        
        coeffs(indexNew,:) = coeff;
        frequencies(indexNew) = f;
        
        labels(indexNew) = signalStruct(i).class;
        positions(indexNew) = signalStruct(i).anatomicalPosition;
        heartRate(indexNew) = signalStruct(i).heartRate;
        waveForm(indexNew) = signalStruct(i).waveForm;
        
        fprintf("%d %d %d\n",indexNew,startIndex,endIndex);
    end
end
toc

preprocessedStruct = struct('coeffs',coeffs,...
                            'frequencies',frequencies,...
                            'meanAbsoluteError',meanAbsoluteError,...
                            'signalSpan',signalSpan,...
                            'signalToNoiseRatio',signalToNoiseRatio,...
                            'labels',labels,...
                            'positions',positions,...
                            'heartRate',heartRate,...
                            'waveForm',waveForm);

%Display some metrics
fprintf("Mean signal to noise ratio %f\n", mean(preprocessedStruct.signalToNoiseRatio));
fprintf("Std signal to noise ratio %f\n", std(preprocessedStruct.signalToNoiseRatio));

fpred = preprocessedStruct.frequencies;
fprintf("Mean percentage error Frequency prediction %f\n",...
    mean(abs(fpred*60-round(fpred*60))./round(fpred*60)));
fprintf("Std percentage error Frequency prediction %f\n",...
    std(abs(fpred*60-round(fpred*60))./round(fpred*60)));

fprintf("Mean of mean absolute error %f\n",...
    mean(preprocessedStruct.meanAbsoluteError));
fprintf("Std of mean absolute error %f\n",...
    std(preprocessedStruct.meanAbsoluteError));

currentFile = mfilename('fullpath');
basepathSave = fileparts(currentFile); 
save(fullfile(basepathSave,"Step1_fourierCoefficients.mat"),'preprocessedStruct');

  