% Load monitor data
basepath = fileread('DatasetPath.txt'); 
signalStruct = MacSimDataLoader.getData(fullfile(basepath,"Dataset"),[1],[16],[1],[]);

%create filter
fs = 1000;
fc_l = 30; width_l = 0.1; attenuation_l = 30;
lpFilt = designfilt('lowpassfir','PassbandFrequency',fc_l/fs*2, ...
      'StopbandFrequency',(fc_l+width_l*fc_l)/fs*2,'PassbandRipple',0.5, ...
      'StopbandAttenuation',attenuation_l,'DesignMethod','kaiserwin');

monitorAverage = zeros(10,1200);
periodTime = 1200;
startPeriod = periodTime*5;
endPeriod = periodTime*45;
for i = 1:length(signalStruct)
    currentData = filtfilt(lpFilt,signalStruct(i).data(startPeriod:(endPeriod-1)));
    monitorAverage(i,:) = mean(reshape(currentData,1200,[]),2);
end

signalStruct2 = MacSimDataLoader.getData(fullfile(basepath,"Dataset"),[3],[16],[1],[]);
aortaAverage = zeros(10,1200);
for i = 1:length(signalStruct)
    currentData = filtfilt(lpFilt,signalStruct2(i).data(startPeriod:(endPeriod-1)));
    aortaAverage(i,:) = mean(reshape(currentData,1200,[]),2);
end


%TrapezCheck
HeartrateInBpm = 50;
HeartPressureAmplitudeInMilimeterHg = 220;
PressureOffsetInMilimeterHg = -100;

cmds = lines(1).*((1:10)'./10);
t = linspace(0,1.2-0.001,1200);

figure('Position',[0 0 500 200]);
%Default
structPara = struct();
structPara.tStartAscend = 0.1;
structPara.tEndAscend = 0.15;
structPara.tStartDescend = 0.45;
structPara.tEndDescend = 0.5;
structPara.windowFraction = 0.1;
subplot(1,2,1);
hold on;
title("Target Ventricle Pressure");
for i = 2:9  
    structPara.tEndAscend = 0.15 + 0.0444*(i-1);
    structPara.tStartDescend = 0.45 + 0.0444*(i-1);
    structPara.tEndDescend = 0.5 + 0.0444*(i-1);
    HeartPressureArrayInMilimeterHg = ...
        PublicationTools.TrapezCurve(HeartrateInBpm, HeartPressureAmplitudeInMilimeterHg,...
        PressureOffsetInMilimeterHg,structPara);

    plot(t,HeartPressureArrayInMilimeterHg,'Color',[cmds(i,:) 0.2]);
end
for i = 1:9:10
    structPara.tEndAscend = 0.15 + 0.0444*(i-1);
    structPara.tStartDescend = 0.45 + 0.0444*(i-1);
    structPara.tEndDescend = 0.5 + 0.0444*(i-1);
    HeartPressureArrayInMilimeterHg = ...
        PublicationTools.TrapezCurve(HeartrateInBpm, HeartPressureAmplitudeInMilimeterHg,...
        PressureOffsetInMilimeterHg,structPara);
   
    plot(t,HeartPressureArrayInMilimeterHg,'Color',cmds(i,:));
end

xlabel("t/s");
ylabel("p/mmHg");
ylim([-150,150]);
set(gca, 'YGrid', 'on', 'XGrid', 'off')


subplot(1,2,2);
hold on;
title("Measured Aortic Pressure");
for i = 2:9
    plot(t,aortaAverage(i,:),'Color',[cmds(i,:) 0.2]);
end
for i = 1:9:10
    plot(t,aortaAverage(i,:),'Color',cmds(i,:));
end

xlabel("t/s");
ylabel("p/mmHg");;
ylim([0,150]);
set(gca, 'YGrid', 'on', 'XGrid', 'off')

